import {BTN_LOADING_FALSE, BTN_LOADING_TRUE, GET_BLOGS, LOADING_FALSE, LOADING_TRUE} from "../actionTypes/actionsTypes";
import {PATH_NAME_AUTH, TOKEN_NAME, USER} from "../../tools/constant";
import axios from "axios";
import {toast} from "react-toastify";

export const getBlogs = (current_page) => async (dispatch) => {

    dispatch({
        type: LOADING_TRUE
    });

    try{

        const token = JSON.parse(localStorage.getItem(TOKEN_NAME));

        const config = {
            headers: {
                'Authorization': 'Bearer ' + token
            }
        };

        const res = await axios.get(`${PATH_NAME_AUTH}/blog/?page=${current_page}`, config);

        dispatch({
            type: GET_BLOGS,
            payload: {
                blogs: res.data.results,
                numberOfPages: Math.ceil(res.data.count/5)
            }
        });

        dispatch({
            type: LOADING_FALSE
        });

    } catch (e) {

        if (e.response.status == 401){
            localStorage.removeItem(TOKEN_NAME);
            localStorage.removeItem(USER);
            dispatch({
                type: LOGIN_OUT
            })
        }

        dispatch({
            type: LOADING_FALSE
        });

    }

};

export const addBlog = (newBlogData, closeAddModalFunction, setCurrentPage) => async (dispatch) => {

    dispatch({
        type: BTN_LOADING_TRUE
    });

    try{

        const token = JSON.parse(localStorage.getItem(TOKEN_NAME));

        const config = {
            headers: {
                'Authorization': 'Bearer ' + token
            }
        };

        const res = await axios.post(`${PATH_NAME_AUTH}/blog/create-blog/`, newBlogData, config);

        toast.success("Blog muvaffaqiyatli yaratildi !!!");

        closeAddModalFunction();

        dispatch(getBlogs(1));

        setCurrentPage(1);

        dispatch({
            type: BTN_LOADING_FALSE
        });

    } catch (e) {

        dispatch({
            type: BTN_LOADING_FALSE
        });

        if (e.response.status == 401){
            localStorage.removeItem(TOKEN_NAME);
            localStorage.removeItem(USER);
            dispatch({
                type: LOGIN_OUT
            })
        }

    }

};

export const updateBlog = (blogID, updatedBlogData, closeModal, setCurrentPage) => async (dispatch) => {

    dispatch({
        type: BTN_LOADING_TRUE
    });

    try{

        const token = JSON.parse(localStorage.getItem(TOKEN_NAME));

        const config = {
            headers: {
                'Authorization': 'Bearer ' + token
            }
        };

        const res = await axios.patch(`${PATH_NAME_AUTH}/blog/update-blog/${blogID}/`, updatedBlogData, config);


        toast.success("Blog muvaffaqiyatli o'zgartirildi !!!");

        closeModal();

        dispatch(getBlogs(1));

        setCurrentPage(1);

        dispatch({
            type: BTN_LOADING_FALSE
        });

    } catch (e) {

        dispatch({
            type: BTN_LOADING_FALSE
        });

        if (e.response.status == 401){
            localStorage.removeItem(TOKEN_NAME);
            localStorage.removeItem(USER);
            dispatch({
                type: LOGIN_OUT
            })
        }

    }

};

export const deleteBlog = (blogID, closeModal, setCurrentPage) => async (dispatch) => {

    dispatch({
        type: BTN_LOADING_TRUE
    });

    try{

        const token = JSON.parse(localStorage.getItem(TOKEN_NAME));

        const config = {
            headers: {
                'Authorization': 'Bearer ' + token
            }
        };

        const res = await axios.delete(`${PATH_NAME_AUTH}/blog/delete-blog/${blogID}/`, config);

        toast.success("Blog muvaffaqiyatli o'chirildi !!!");

        closeModal();

        dispatch(getBlogs(1));

        setCurrentPage(1);

        dispatch({
            type: BTN_LOADING_FALSE
        });

    } catch (e) {

        toast.success("Blog muvaffaqiyatli o'chirilmadi !!!");

        dispatch({
            type: BTN_LOADING_FALSE
        });

        if (e.response.status == 401){
            localStorage.removeItem(TOKEN_NAME);
            localStorage.removeItem(USER);
            dispatch({
                type: LOGIN_OUT
            })
        }

    }

};