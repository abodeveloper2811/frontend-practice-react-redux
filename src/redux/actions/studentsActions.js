import {
    ADD_STUDENT,
    DELETE_STUDENT, EDIT_STUDENT,
    INCREMENT,
    SET_DELETE_STUDENT_ID,
    SET_UPDATE_STUDENT_ID
} from "../actionTypes/actionsTypes";
import {toast} from "react-toastify";

export const addStudent = (studentData, closeAddModalFunction) => (dispatch) => {

    dispatch({
        type: ADD_STUDENT,
        payload: {
            studentData
        }
    });

    toast.success("Yangi talaba yaratildi !!!");

    closeAddModalFunction();

};

export const setDeleteStudentId = (studentID) => (dispatch) => {

    dispatch({
        type: SET_DELETE_STUDENT_ID,
        payload: {
            studentID
        }
    });

};

export const deleteStudent = (studentID, closeModal) => (dispatch) => {

    dispatch({
        type: DELETE_STUDENT,
        payload: {
            studentID
        }
    });

    toast.success("Talaba o'chirildi !!!");

    closeModal();
};

export const setUpdateStudentId = (studentID) => (dispatch) => {

    dispatch({
        type: SET_UPDATE_STUDENT_ID,
        payload: {
            studentID
        }
    });

};

export const editStudent = (studentID, studentData, closeModal) => (dispatch) => {

    dispatch({
        type: EDIT_STUDENT,
        payload: {
            studentID,
            studentData
        }
    });

    toast.success("Talaba ma'lumoti tahrirlandi !!!");

    closeModal();

};
