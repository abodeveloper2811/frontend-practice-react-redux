import {
    BTN_LOADING_FALSE,
    BTN_LOADING_TRUE, GET_PRODUCT,
    GET_PRODUCTS, LOADING_FALSE, LOADING_TRUE,
    REMOVE_DELETE_PRODUCT,
    REMOVE_UPDATE_PRODUCT,
    SET_DELETE_PRODUCT,
    SET_UPDATE_PRODUCT
} from "../actionTypes/actionsTypes";
import axios from "axios";
import {toast} from "react-toastify";

export const getProduct = (product_id) => (dispatch) => {

    dispatch({
        type: LOADING_TRUE
    });

    axios.get(`http://localhost:3002/products/${product_id}`)

        .then((res) => {

            setTimeout(()=>{
                dispatch({
                    type: GET_PRODUCT,
                    payload: {
                        product: res.data
                    }
                })
            }, 1000);

        })
        .catch((err) => {
            console.log(err);
            dispatch({
                type: LOADING_FALSE
            });
        })

};

export const getProducts = (search_text, limit, page) => (dispatch) => {

    dispatch({
        type: LOADING_TRUE
    });

    axios.get(`http://localhost:3002/products?q=${search_text}&_limit=${limit}&_page=${page}`)

        .then((res) => {

            setTimeout(()=>{
                dispatch({
                    type: GET_PRODUCTS,
                    payload: {
                        products: res.data,
                        numberOfPages: Math.ceil(res.headers['x-total-count'] / limit)
                    }
                })
            }, 1000);

        })
        .catch((err) => {
            console.log(err);
            dispatch({
                type: LOADING_FALSE
            });
        })

};

export const addProduct = (newProductData, closeAddModalFunction, current_page, limit) => (dispatch) => {

    dispatch({
        type: BTN_LOADING_TRUE
    });

    axios.post(`http://localhost:3002/products`, newProductData)

        .then((res) => {

            setTimeout(()=>{
                closeAddModalFunction();

                dispatch(getProducts('', limit, current_page));

                toast.success("Mahsulot muvaffaqiyatli yaratildi !!!");

                dispatch({
                    type: BTN_LOADING_FALSE
                });

            },1000);

        })
        .catch((err) => {
            console.log(err);
            toast.error("Mahsulot yaratilmadi !!!");
            dispatch({
                type: LOADING_FALSE
            });
            dispatch({
                type: BTN_LOADING_FALSE
            });
        })

};

export const setDeleteProduct = (delete_product) => (dispatch) => {

    dispatch({
        type: SET_DELETE_PRODUCT,
        payload: delete_product
    })

};

export const deleteProduct = (delete_product_id, limit, setCurrentPage) => (dispatch) => {

    dispatch({
        type: BTN_LOADING_TRUE
    });

    axios.delete(`http://localhost:3002/products/${delete_product_id}`)

        .then((res) => {

            setTimeout(()=>{
                dispatch({
                    type: REMOVE_DELETE_PRODUCT
                });

                dispatch(getProducts('', limit, 1));

                setCurrentPage(1);

                toast.success("Mahsulot muvaffaqiyatli o'chirildi !!!");

                dispatch({
                    type: BTN_LOADING_FALSE
                })
            },1000);

        })
        .catch((err) => {
            console.log(err);
            toast.error("Mahsulot o'chirilmadi !!!");
            dispatch({
                type: BTN_LOADING_FALSE
            })
        })

};

export const setUpdateProduct = (update_product) => (dispatch) => {

    dispatch({
        type: SET_UPDATE_PRODUCT,
        payload: update_product
    })

};

export const updateProduct = (update_product_id, update_product_data, limit, current_page) => (dispatch) => {

    dispatch({
        type: BTN_LOADING_TRUE
    });

    axios.put(`http://localhost:3002/products/${update_product_id}/`, update_product_data)

        .then((res) => {

            setTimeout(()=>{
                dispatch({
                    type: REMOVE_UPDATE_PRODUCT
                });

                dispatch(getProducts('', limit, current_page));

                toast.success("Mahsulot ma'lumoti muvaffaqiyatli o'zgartirildi !!!");

                dispatch({
                    type: BTN_LOADING_FALSE
                })
            },1000);

        })
        .catch((err) => {
            console.log(err);
            toast.error("Mahsulot ma'lumoti o'zgartirilmadi !!!");
            dispatch({
                type: BTN_LOADING_FALSE
            })
        })

};

