import axios from "axios";
import {PATH_NAME_AUTH, TOKEN_NAME, USER} from "../../tools/constant";
import {
    BTN_LOADING_FALSE,
    BTN_LOADING_TRUE,
    LOGIN_FAIL,
    LOGIN_SUCCESS, REGISTER_FAIL,
    REGISTER_SUCCESS
} from "../actionTypes/actionsTypes";
import {toast} from "react-toastify";

export const SignIn = (userData, history) => async (dispatch) => {

    dispatch({
       type: BTN_LOADING_TRUE
    });

    try{

        const res = await axios.post(`${PATH_NAME_AUTH}/account/login/`, userData);

        dispatch({
            type: LOGIN_SUCCESS,
            payload: {
                accessToken: res.data.token.access,
                user: userData
            }
        });

        dispatch({
            type: BTN_LOADING_FALSE
        });

        localStorage.setItem(TOKEN_NAME, JSON.stringify(res.data.token.access));
        localStorage.setItem(USER, JSON.stringify(userData));

        history.push("/home");

    } catch (e) {

        toast.error("Login yoki parol xato !!!");

        dispatch({
            type: BTN_LOADING_FALSE
        });

        dispatch({
            type: LOGIN_FAIL,
            payload: {
                error: e.message
            }
        });

    }

};

export const SignUp = (newUserData, history) => async (dispatch) => {

    dispatch({
        type: BTN_LOADING_TRUE
    });

    try{

        const res = await axios.post(`${PATH_NAME_AUTH}/account/register/`, newUserData);

        dispatch({
            type: REGISTER_SUCCESS,
            payload: {
                accessToken: res.data.token.access,
                user: newUserData
            }
        });

        dispatch({
            type: BTN_LOADING_FALSE
        });

        localStorage.setItem(TOKEN_NAME, JSON.stringify(res.data.token.access));
        localStorage.setItem(USER, JSON.stringify(newUserData));

        history.push("/home");

    } catch (e) {

        dispatch({
            type: BTN_LOADING_FALSE
        });

        toast.error("Ro'yxatdan o'tib bo'lmadi !!!");

        dispatch({
            type: REGISTER_FAIL,
            payload: {
                error: e.message
            }
        });

    }

};

export const LogOut = () => async (dispatch) => {

    try{

        localStorage.removeItem(TOKEN_NAME);
        localStorage.removeItem(USER);

    } catch (e) {

    }

};