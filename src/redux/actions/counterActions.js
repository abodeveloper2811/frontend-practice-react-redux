import {DECREMENT, INCREMENT, RESET} from "../actionTypes/actionsTypes";

export const Increment = (step) => (dispatch) => {

    dispatch({
        type: INCREMENT,
        payload: step
    });

};

export const Decrement = (step) => (dispatch) => {

    dispatch({
        type: DECREMENT,
        payload: step
    });

};

export const Reset = () => (dispatch) => {

    dispatch({
        type: RESET,
    });

};



