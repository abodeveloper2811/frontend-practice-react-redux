import {
    BTN_LOADING_FALSE,
    BTN_LOADING_TRUE,
    GET_BLOGS,
    LOADING_FALSE,
    LOADING_TRUE, REMOVE_DELETE_BLOG, REMOVE_UPDATE_BLOG, SET_DELETE_BLOG,
    SET_UPDATE_BLOG
} from "../actionTypes/actionsTypes";

const initialState = {
    blogs: [],
    blog: {},
    numberOfPages: 1,

    update_blog: null,
    delete_blog: null,

    loading: false,
    btn_loading: false
};

export const blogsReducer = (state = initialState, action) => {

    const {type, payload} = action;

    switch (type) {


        case BTN_LOADING_TRUE:
            return {
                ...state,
                btn_loading: true,
            };

        case BTN_LOADING_FALSE:
            return {
                ...state,
                btn_loading: false,
            };

        case LOADING_TRUE:
            return {
                ...state,
                loading: true,
            };

        case LOADING_FALSE:
            return {
                ...state,
                loading: false,
            };

        case GET_BLOGS:
            return {
                ...state,
                blogs: payload.blogs,
                numberOfPages: payload.numberOfPages,
            };

        case SET_UPDATE_BLOG:
            return {
                ...state,
                update_blog: payload
            };

        case SET_DELETE_BLOG:
            return {
                ...state,
                delete_blog: payload
            };

        case REMOVE_UPDATE_BLOG:
            return {
                ...state,
                update_blog: null
            };

        case REMOVE_DELETE_BLOG:
            return {
                ...state,
                delete_blog: null
            };


        default:
            return state;
    }
};
