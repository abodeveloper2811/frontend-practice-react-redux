import {combineReducers} from "redux";
import {counterReducer} from "./counterReducer";
import {studentsReducer} from "./studentsReducer";
import {productsReducer} from "./productsReducer";
import {authReducer} from "./authReducer";
import {blogsReducer} from "./blogsReducer";

export const rootReducer = combineReducers({
    auth: authReducer,
    counter: counterReducer,
    students: studentsReducer,
    products: productsReducer,
    blogs: blogsReducer
});
