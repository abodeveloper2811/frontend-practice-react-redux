import {
    ADD_STUDENT,
    DELETE_STUDENT, EDIT_STUDENT,
    INCREMENT,
    REMOVE_DELETE_STUDENT, REMOVE_UPDATE_STUDENT,
    SET_DELETE_STUDENT_ID, SET_UPDATE_STUDENT_ID
} from "../actionTypes/actionsTypes";

const initialState = {

    students: localStorage.getItem("STUDENTS_DATA") !== null ? JSON.parse(localStorage.getItem("STUDENTS_DATA")) : [],

    delete_student: null,
    update_student: null,

    loading: false,
};

export const studentsReducer = (state = initialState, action) => {

    const {type, payload} = action;

    switch (type) {

        case ADD_STUDENT:

            let newData = [...state.students];
            newData.push(payload.studentData);
            localStorage.setItem("STUDENTS_DATA", JSON.stringify(newData));

            return {
                ...state,
                students: newData,
            };

        case SET_DELETE_STUDENT_ID:

            return {
                ...state,
                delete_student: state.students.find(student=> student.id === payload.studentID)
            };

        case REMOVE_DELETE_STUDENT:

            return {
                ...state,
                delete_student: null
            };

        case DELETE_STUDENT:

            localStorage.setItem("STUDENTS_DATA", JSON.stringify(state.students.filter(student=> student.id !== payload.studentID)));

            return {
                ...state,
                students: state.students.filter(student=> student.id !== payload.studentID)
            };

        case SET_UPDATE_STUDENT_ID:

            return {
                ...state,
                update_student: state.students.find(student=> student.id === payload.studentID)
            };

        case REMOVE_UPDATE_STUDENT:

            return {
                ...state,
                update_student: null
            };

        case EDIT_STUDENT:

            let newEditedData = [...state.students];
            let editedIndex = state.students.indexOf(state.students.find(student=>student.id === payload.studentID));

            newEditedData[editedIndex] = payload.studentData;

            localStorage.setItem("STUDENTS_DATA", JSON.stringify(newEditedData));

            return {
                ...state,
                students: newEditedData
            };

        default:
            return state;
    }
};
