import {DECREMENT, INCREMENT, RESET} from "../actionTypes/actionsTypes";

const initialState = {
    count: 0,
    loading: false
};

export const counterReducer = (state = initialState, action) => {

    const {type, payload} = action;

    switch (type) {

        case INCREMENT:
            return {
                ...state,
                count: state.count + payload,
            };

        case DECREMENT:
            return {
                ...state,
                count: state.count - payload,
            };

        case RESET:
            return {
                ...state,
                count: 0,
            };


        default:
            return state;
    }
};
