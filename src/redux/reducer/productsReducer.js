import {
    BTN_LOADING_FALSE,
    BTN_LOADING_TRUE, GET_PRODUCT,
    GET_PRODUCTS, LOADING_FALSE, LOADING_TRUE,
    REMOVE_DELETE_PRODUCT,
    REMOVE_UPDATE_PRODUCT,
    SET_DELETE_PRODUCT,
    SET_UPDATE_PRODUCT
} from "../actionTypes/actionsTypes";

const initialState = {

    products: [],
    product: {},
    numberOfPages: 1,

    delete_product: null,
    update_product: null,

    loading: false,
    btn_loading: false,
};

export const productsReducer = (state = initialState, action) => {

    const {type, payload} = action;

    switch (type) {

        case LOADING_TRUE:

            return {
                ...state,
                loading: true
            };

        case LOADING_FALSE:

            return {
                ...state,
                loading: false
            };

        case BTN_LOADING_TRUE:

            return {
                ...state,
                btn_loading: true
            };

        case BTN_LOADING_FALSE:

            return {
                ...state,
                btn_loading: false
            };

        case GET_PRODUCT:

            return {
                ...state,
                product: payload.product,
                loading: false
            };

        case GET_PRODUCTS:

            return {
                ...state,
                products: payload.products,
                numberOfPages: payload.numberOfPages,
                loading: false
            };

        case SET_DELETE_PRODUCT:

            return {
                ...state,
                delete_product: payload
            };

        case REMOVE_DELETE_PRODUCT:

            return {
                ...state,
                delete_product: null
            };

        case SET_UPDATE_PRODUCT:

            return {
                ...state,
                update_product: payload
            };

        case REMOVE_UPDATE_PRODUCT:

            return {
                ...state,
                update_product: null
            };

        default:
            return state;
    }
};
