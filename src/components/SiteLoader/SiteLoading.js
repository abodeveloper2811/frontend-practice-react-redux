import React from 'react';
import Lottie from 'react-lottie';
import animation from './data.json';

const SiteLoading = () => {

    const options= {
        loop: true,
        autoplay: true,
        prerender: true,
        animationData: animation
    };

    return (
        <div className="Loader bg-dark" style={{height: "100vh"}}>
            <Lottie options={options} segments={[0, 0]} />
        </div>
    );
};

export default SiteLoading;