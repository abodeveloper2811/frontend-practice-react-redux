import React, {useState} from 'react';
import {Modal, ModalBody, ModalHeader} from "reactstrap";
import {AvField, AvForm} from "availity-reactstrap-validation";
import {useDispatch, useSelector} from "react-redux";
import {REMOVE_UPDATE_STUDENT} from "../../redux/actionTypes/actionsTypes";
import {editStudent} from "../../redux/actions/studentsActions";

const EditStudent = () => {

    const dispatch = useDispatch();

    const {update_student} = useSelector(state => state.students);

    const closeModal =()=>{
      dispatch({
          type: REMOVE_UPDATE_STUDENT
      })
    };

    const editStudentFunction =(event, studentdata)=>{
        dispatch(editStudent(update_student?.id, studentdata, closeModal()))
    };

    let condition = false;
    if (update_student !==null){
        condition = true;
    }

    return (
        <div className="Add-student">
            <Modal isOpen={condition} toggle={closeModal}>
                <ModalHeader className="bg-warning fw-bold text-white" toggle={closeModal}>Talabani tahrirlash</ModalHeader>
                <ModalBody>

                    <AvForm model={update_student} onValidSubmit={editStudentFunction}>

                        <AvField
                            type="text"
                            name="firstName"
                            label="Ismi"
                            required
                            validate={{
                                required: {value: true, errorMessage: 'Talaba ismini kiriting !'}
                            }}
                            className="mb-2"
                        />

                        <AvField
                            type="text"
                            name="lastName"
                            label="Familiyasi"
                            required
                            validate={{
                                required: {value: true, errorMessage: 'Talaba familiyasini kiriting !'}
                            }}
                            className="mb-2"
                        />

                        <AvField
                            type="number"
                            name="age"
                            label="Yoshi"
                            required
                            validate={{
                                required: {value: true, errorMessage: 'Talaba yoshini kiriting !'}
                            }}
                            className="mb-2"
                        />

                        <AvField
                            type="select"
                            name="status"
                            label="O'qish turi"
                            validate={{
                                required: {value: true, errorMessage: "O'qish turini tanlang !"}
                            }}
                            className="mb-2"
                        >
                            <option value="" selected={true} disabled={true}>Tanlang</option>
                            <option value="Grand">Grand</option>
                            <option value="Kontrakt">Konrakt</option>
                        </AvField>

                        <div className="btn-box mt-3">
                            <button type="button" className="btn btn-info w-50 text-white fw-bold" onClick={closeModal}>Chiqish</button>
                            <button type="submit" className="btn btn-warning w-50 text-white fw-bold">Saqlash</button>
                        </div>

                    </AvForm>

                </ModalBody>

            </Modal>
        </div>
    );
};

export default EditStudent;