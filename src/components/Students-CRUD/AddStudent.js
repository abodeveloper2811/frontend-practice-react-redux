import React from 'react';
import {Button, Modal, ModalBody, ModalFooter, ModalHeader} from "reactstrap";
import {AvForm, AvField} from 'availity-reactstrap-validation';
import {useDispatch} from "react-redux";
import {addStudent} from "../../redux/actions/studentsActions";

const AddStudent = ({open_add_modal, toggle, openAddModalFunction, closeAddModalFunction}) => {

    const dispatch = useDispatch();

    const addStudentSubmit = (event, studentData) => {

        let obj = {
            ...studentData,
            id: `${studentData.firstName}${studentData.lastName}${studentData.age}`,
        };

        dispatch(addStudent(obj, closeAddModalFunction));
    };

    return (
        <div className="Add-student">
            <Modal isOpen={open_add_modal} toggle={toggle}>
                <ModalHeader className="bg-success text-white" toggle={toggle}>Talaba qo'shish</ModalHeader>
                <ModalBody>

                    <AvForm onValidSubmit={addStudentSubmit}>

                        <AvField
                            type="text"
                            name="firstName"
                            label="Ismi"
                            required
                            validate={{
                                required: {value: true, errorMessage: 'Talaba ismini kiriting !'}
                            }}
                            className="mb-2"
                        />

                        <AvField
                            type="text"
                            name="lastName"
                            label="Familiyasi"
                            required
                            validate={{
                                required: {value: true, errorMessage: 'Talaba familiyasini kiriting !'}
                            }}
                            className="mb-2"
                        />

                        <AvField
                            type="number"
                            name="age"
                            label="Yoshi"
                            required
                            validate={{
                                required: {value: true, errorMessage: 'Talaba yoshini kiriting !'}
                            }}
                            className="mb-2"
                        />

                        <AvField
                            type="select"
                            name="status"
                            label="O'qish turi"
                            validate={{
                                required: {value: true, errorMessage: "O'qish turini tanlang !"}
                            }}
                            className="mb-2"
                        >
                            <option value="" selected={true} disabled={true}>Tanlang</option>
                            <option value="Grand">Grand</option>
                            <option value="Kontrakt">Konrakt</option>
                        </AvField>

                        <div className="btn-box mt-3">
                            <button type="button" onClick={closeAddModalFunction} className="btn btn-info w-50 text-white fw-bold">Chiqish</button>
                            <button type="submit" className="btn btn-success w-50 text-white fw-bold">Saqlash</button>
                        </div>

                    </AvForm>

                </ModalBody>

            </Modal>
        </div>
    );
};

export default AddStudent;