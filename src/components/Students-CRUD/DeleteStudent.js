import React, {useState} from 'react';
import {Modal, ModalBody, ModalHeader, ModalFooter} from "reactstrap";
import {useDispatch, useSelector} from "react-redux";
import {REMOVE_DELETE_STUDENT} from "../../redux/actionTypes/actionsTypes";
import {deleteStudent} from "../../redux/actions/studentsActions";

const DeleteStudent = () => {

    const dispatch = useDispatch();

    const {delete_student} = useSelector(state => state.students);

    const closeModal = ()=>{
        dispatch({
            type: REMOVE_DELETE_STUDENT
        })
    };

    const deleteStudentFunction =()=>{
        dispatch(deleteStudent(delete_student?.id, closeModal()));
    };

    let condition = false;
    if (delete_student !== null){
        condition = true;
    }

    return (
        <div className="Delete-Student">
            <Modal isOpen={condition} toggle={closeModal}>
                <ModalHeader className="bg-danger text-white" toggle={closeModal}>Talabani o'chirish</ModalHeader>
                <ModalBody>
                    Siz haqiqatdan ham ushbu <b>{delete_student?.firstName} {delete_student?.lastName}</b> nomli talabani o'chirmoqchimisiz ?
                </ModalBody>
                <ModalFooter>
                    <button className="btn btn-info text-white fw-bold" onClick={closeModal}>YO'Q</button>
                    <button className="btn btn-danger text-white fw-bold" onClick={deleteStudentFunction}>HA</button>
                </ModalFooter>
            </Modal>
        </div>
    );
};

export default DeleteStudent;