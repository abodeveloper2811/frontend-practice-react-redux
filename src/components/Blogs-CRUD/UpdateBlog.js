import React from 'react';
import {Modal, ModalBody, ModalHeader} from "reactstrap";
import {AvField, AvForm} from "availity-reactstrap-validation";
import {useDispatch, useSelector} from "react-redux";
import {REMOVE_UPDATE_BLOG} from "../../redux/actionTypes/actionsTypes";
import {updateBlog} from "../../redux/actions/blogsActions";

const UpdateBlog = ({setCurrentPage}) => {

    const dispatch = useDispatch();

    const {update_blog, btn_loading} = useSelector(state => state.blogs);

    const closeModal =()=>{
        dispatch({
            type: REMOVE_UPDATE_BLOG
        })
    };

    const updateBlogFunction =(event, updateBlogData)=>{
        dispatch(updateBlog(update_blog?.id, updateBlogData, closeModal, setCurrentPage));
    };

    let condition = false;
    if (update_blog !== null){
        condition = true;
    }

    return (
        <div className="Update-blog">
            <Modal isOpen={condition} toggle={closeModal}>
                <ModalHeader className="bg-warning fw-bold text-white" toggle={closeModal}>Blogni tahrirlash</ModalHeader>
                <ModalBody>

                    <AvForm model={update_blog} onValidSubmit={updateBlogFunction}>

                        <AvField
                            type="text"
                            name="title"
                            label="Sarlavha"
                            required
                            validate={{
                                required: {value: true, errorMessage: 'Sarlavhani kiriting !'}
                            }}
                            className="mb-2"
                        />

                        <AvField
                            type="textarea"
                            name="description"
                            label="Tavsif"
                            required
                            validate={{
                                required: {value: true, errorMessage: 'Tavsifni kiriting !'}
                            }}
                            className="mb-2"
                        />


                        <AvField
                            type="text"
                            name="thumbnail"
                            label="Blog uchun rasm"
                            required
                            validate={{
                                required: {value: true, errorMessage: 'Blog rasmini kiriting !'}
                            }}
                            className="mb-2"
                        />

                        <div className="btn-box mt-3">
                            <button type="button" onClick={closeModal} className="btn btn-info w-50 text-white fw-bold">Chiqish</button>
                            <button type="submit" className="btn btn-warning w-50 text-white fw-bold" disabled={btn_loading}>Saqlash</button>
                        </div>

                    </AvForm>

                </ModalBody>

            </Modal>
        </div>
    );
};

export default UpdateBlog;