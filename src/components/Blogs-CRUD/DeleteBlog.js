import React from 'react';
import {Modal, ModalBody, ModalFooter, ModalHeader} from "reactstrap";
import {useDispatch, useSelector} from "react-redux";
import {REMOVE_DELETE_BLOG} from "../../redux/actionTypes/actionsTypes";
import {deleteBlog} from "../../redux/actions/blogsActions";

const DeleteBlog = ({setCurrentPage}) => {

    const dispatch = useDispatch();

    const {delete_blog} = useSelector(state => state.blogs);

    const closeModal =()=>{
      dispatch({
          type: REMOVE_DELETE_BLOG
      })
    };

    const deleteBlogFunction =()=>{
      dispatch(deleteBlog(delete_blog?.id, closeModal, setCurrentPage));
    };

    let condition = false;
    if (delete_blog !== null){
        condition = true;
    }

    return (
        <div className="Delete-Blog">
            <Modal isOpen={condition} toggle={closeModal}>
                <ModalHeader className="bg-danger text-white" toggle={closeModal}>Blogni o'chirish</ModalHeader>
                <ModalBody>
                    Siz haqiqatdan ham ushbu <b>{delete_blog?.title}</b> nomli blogni o'chirmoqchimisiz ?
                </ModalBody>
                <ModalFooter>
                    <button className="btn btn-info text-white fw-bold" onClick={closeModal}>YO'Q</button>
                    <button className="btn btn-danger text-white fw-bold" onClick={deleteBlogFunction}>HA</button>
                </ModalFooter>
            </Modal>
        </div>
    );
};

export default DeleteBlog;