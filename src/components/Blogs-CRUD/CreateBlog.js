import React from 'react';
import {Button, Modal, ModalBody, ModalHeader} from "reactstrap";
import {AvForm, AvField} from 'availity-reactstrap-validation';
import {useDispatch, useSelector} from "react-redux";
import {addBlog} from "../../redux/actions/blogsActions";

const CreateBlog = ({setCurrentPage, closeAddModalFunction, show_add_modal}) => {

    const dispatch = useDispatch();

    const {btn_loading} = useSelector(state => state.blogs);

    const createBlogFunction =(event, newBlogData)=>{
        dispatch(addBlog(newBlogData, closeAddModalFunction, setCurrentPage));
    };

    return (
        <div className="Add-blog">
            <Modal isOpen={show_add_modal} toggle={closeAddModalFunction}>
                <ModalHeader className="bg-success text-white" toggle={closeAddModalFunction}>Blog yaratish</ModalHeader>
                <ModalBody>

                    <AvForm onValidSubmit={createBlogFunction}>

                        <AvField
                            type="text"
                            name="title"
                            label="Sarlavha"
                            required
                            validate={{
                                required: {value: true, errorMessage: 'Sarlavhani kiriting !'}
                            }}
                            className="mb-2"
                        />

                        <AvField
                            type="textarea"
                            name="description"
                            label="Tavsif"
                            required
                            validate={{
                                required: {value: true, errorMessage: 'Tavsifni kiriting !'}
                            }}
                            className="mb-2"
                        />


                        <AvField
                            type="text"
                            name="thumbnail"
                            label="Blog uchun rasm"
                            required
                            validate={{
                                required: {value: true, errorMessage: 'Blog rasmini kiriting !'}
                            }}
                            className="mb-2"
                        />

                        <div className="btn-box mt-3">
                            <button type="button" onClick={closeAddModalFunction} className="btn btn-info w-50 text-white fw-bold">Chiqish</button>
                            <button type="submit" disabled={btn_loading} className="btn btn-success w-50 text-white fw-bold">Saqlash</button>
                        </div>

                    </AvForm>

                </ModalBody>

            </Modal>
        </div>
    );
};

export default CreateBlog;