import React from 'react';
import {Modal, ModalBody, ModalFooter, ModalHeader} from "reactstrap";
import {useDispatch, useSelector} from "react-redux";
import {REMOVE_DELETE_PRODUCT} from "../../redux/actionTypes/actionsTypes";
import {deleteProduct} from "../../redux/actions/productsActions";

const DeleteProduct = ({limit, setCurrentPage}) => {

    const dispatch = useDispatch();

    const {delete_product, btn_loading} = useSelector(state => state.products);

    const closeModal = () => {
        dispatch({
            type: REMOVE_DELETE_PRODUCT
        })
    };

    const deleteProductFunction =()=>{
        dispatch(deleteProduct(delete_product?.id, limit, setCurrentPage));
    };

    let condition = false;
    if (delete_product !== null) {
        condition = true
    }

    return (
        <div className="Delete-Student">
            <Modal isOpen={condition} toggle={closeModal}>
                <ModalHeader className="bg-danger text-white" toggle={closeModal}>Mahsulotni o'chirish</ModalHeader>
                <ModalBody>
                    Siz haqiqatdan ham ushbu <b>{delete_product?.name}</b> nomli mahsulotni o'chirmoqchimisiz ?
                </ModalBody>
                <ModalFooter>
                    <button className="btn btn-info text-white fw-bold" onClick={closeModal}>YO'Q</button>
                    <button className="btn btn-danger text-white fw-bold" onClick={deleteProductFunction} disabled={btn_loading}>HA</button>
                </ModalFooter>
            </Modal>
        </div>
    );
};

export default DeleteProduct;