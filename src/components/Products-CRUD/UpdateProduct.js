import React from 'react';
import {Modal, ModalBody, ModalHeader} from "reactstrap";
import {AvField, AvForm} from "availity-reactstrap-validation";
import {useDispatch, useSelector} from "react-redux";
import {REMOVE_UPDATE_PRODUCT} from "../../redux/actionTypes/actionsTypes";
import {updateProduct} from "../../redux/actions/productsActions";

const UpdateProduct = ({limit, current_page}) => {

    const dispatch = useDispatch();

    const {update_product, btn_loading} = useSelector(state => state.products);

    const closeModal =()=>{
      dispatch({
          type: REMOVE_UPDATE_PRODUCT
      })
    };

    const updateProductFunction =(event, updateProductData)=>{
        dispatch(updateProduct(update_product?.id, updateProductData, limit, current_page))
    };

    let condition = false;
    if (update_product !== null){
        condition = true;
    }

    return (
        <div className="Add-student">
            <Modal isOpen={condition} toggle={closeModal}>
                <ModalHeader className="bg-warning fw-bold text-white" toggle={closeModal}>Mashulotni tahrirlash</ModalHeader>
                <ModalBody>

                    <AvForm model={update_product} onValidSubmit={updateProductFunction}>

                        <AvField
                            type="text"
                            name="name"
                            label="Mahsulot nomi"
                            required
                            validate={{
                                required: {value: true, errorMessage: 'Mahsulot nomini kiriting !'}
                            }}
                            className="mb-2"
                        />

                        <AvField
                            type="select"
                            name="format"
                            label="Mahsulot formati"
                            required
                            validate={{
                                required: {value: true, errorMessage: 'Mahsulot formatini tanlang !'}
                            }}
                            className="mb-2"
                        >
                            <option disabled={true} selected={true} value="">Mahsulot formatini tanlang</option>
                            <option value="kg">Kg</option>
                            <option value="dona">Dona</option>
                            <option value="litr">Litr</option>
                        </AvField>

                        <AvField
                            type="number"
                            name="price"
                            label="Narxi"
                            required
                            validate={{
                                required: {value: true, errorMessage: 'Mahsulot narxini kiriting !'}
                            }}
                            className="mb-2"
                        />

                        <AvField
                            type="text"
                            name="image"
                            label="Mahsulot rasmi"
                            required
                            validate={{
                                required: {value: true, errorMessage: 'Mahsulot rasmini kiriting !'}
                            }}
                            className="mb-2"
                        />

                        <div className="btn-box mt-3">
                            <button type="button" onClick={closeModal} className="btn btn-info w-50 text-white fw-bold">Chiqish</button>
                            <button type="submit" className="btn btn-warning w-50 text-white fw-bold" disabled={btn_loading}>Saqlash</button>
                        </div>

                    </AvForm>

                </ModalBody>

            </Modal>
        </div>
    );
};

export default UpdateProduct;