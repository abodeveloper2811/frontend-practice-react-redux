import React from 'react';
import {Button, Modal, ModalBody, ModalFooter, ModalHeader} from "reactstrap";
import {AvForm, AvField} from 'availity-reactstrap-validation';
import {useDispatch, useSelector} from "react-redux";
import {addProduct} from "../../redux/actions/productsActions";

const CreateProduct = ({current_page, limit, add_open_modal, toggle, closeAddModalFunction}) => {

    const dispatch = useDispatch();

    const {btn_loading} = useSelector(state => state.products);

    const addProductFunction =(event, newProductData)=>{
        dispatch(addProduct(newProductData, closeAddModalFunction, current_page, limit));
    };

    return (
        <div className="Add-student">
            <Modal isOpen={add_open_modal} toggle={toggle}>
                <ModalHeader className="bg-success text-white" toggle={toggle}>Mahsulot qo'shish</ModalHeader>
                <ModalBody>

                    <AvForm onValidSubmit={addProductFunction}>

                        <AvField
                            type="text"
                            name="name"
                            label="Mahsulot nomi"
                            required
                            validate={{
                                required: {value: true, errorMessage: 'Mahsulot nomini kiriting !'}
                            }}
                            className="mb-2"
                        />

                        <AvField
                            type="select"
                            name="format"
                            label="Mahsulot formati"
                            required
                            validate={{
                                required: {value: true, errorMessage: 'Mahsulot formatini tanlang !'}
                            }}
                            className="mb-2"
                        >
                            <option disabled={true} selected={true} value="">Mahsulot formatini tanlang</option>
                            <option value="kg">Kg</option>
                            <option value="dona">Dona</option>
                            <option value="litr">Litr</option>
                        </AvField>

                        <AvField
                            type="number"
                            name="price"
                            label="Narxi"
                            required
                            validate={{
                                required: {value: true, errorMessage: 'Mahsulot narxini kiriting !'}
                            }}
                            className="mb-2"
                        />

                        <AvField
                            type="text"
                            name="image"
                            label="Mahsulot rasmi"
                            required
                            validate={{
                                required: {value: true, errorMessage: 'Mahsulot rasmini kiriting !'}
                            }}
                            className="mb-2"
                        />

                        <div className="btn-box mt-3">
                            <button type="button" onClick={closeAddModalFunction} className="btn btn-info w-50 text-white fw-bold">Chiqish</button>
                            <button type="submit" disabled={btn_loading} className="btn btn-success w-50 text-white fw-bold">Saqlash</button>
                        </div>

                    </AvForm>

                </ModalBody>

            </Modal>
        </div>
    );
};

export default CreateProduct;