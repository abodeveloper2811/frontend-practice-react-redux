import React from 'react';
import "./Layout.scss";
import DashboardIcon from '@mui/icons-material/Dashboard';
import {NavLink} from "react-router-dom";
import IsoIcon from '@mui/icons-material/Iso';
import ExposureIcon from '@mui/icons-material/Exposure';
import SchoolIcon from '@mui/icons-material/School';
import ShoppingCartIcon from '@mui/icons-material/ShoppingCart';
import GroupsIcon from '@mui/icons-material/Groups';
import PostAddIcon from '@mui/icons-material/PostAdd';
import LogoutIcon from '@mui/icons-material/Logout';
import MarkEmailReadIcon from '@mui/icons-material/MarkEmailRead';
import {BsPersonSquare} from "react-icons/bs";
import {useDispatch, useSelector} from "react-redux";
import {LogOut} from "../../redux/actions/authActions";

const Layout = ({children}) => {

    const dispatch = useDispatch();

    const {user} = useSelector(state => state.auth);

    const sidebarData = [
        {
            icon: <DashboardIcon/>,
            title: "Dashboard",
            pathname: '/home'
        },
        {
            icon: <IsoIcon/>,
            title: "Counter with Connect",
            pathname: '/connect-counter'
        },
        {
            icon: <ExposureIcon/>,
            title: "Counter with Hook",
            pathname: '/hook-counter'
        },
        {
            icon: <SchoolIcon/>,
            title: "Students CRUD with localhost",
            pathname: '/students-crud'
        },
        {
            icon: <ShoppingCartIcon/>,
            title: "Products CRUD with json-server",
            pathname: '/products-crud'
        },
        {
            icon: <PostAddIcon/>,
            title: "Blogs CRUD with JWT TOKEN",
            pathname: '/blogs-crud'
        },
        {
            icon: <MarkEmailReadIcon/>,
            title: "Send message to Telegram API",
            pathname: '/send-message-to-telegram'
        },
        {
            icon: <MarkEmailReadIcon/>,
            title: "Animations in React with AOS",
            pathname: '/animations-with-aos'
        },
        {
            icon: <MarkEmailReadIcon/>,
            title: "Animations in React with Reveal",
            pathname: '/animations-with-reveal'
        }
    ];

    const LogOutFunction =()=>{
        dispatch(LogOut());
    };

    return (
        <div className="Layout">

            <div className="sidebar">

                <div className="name-box">
                    <div className="img-box">
                        <BsPersonSquare style={{fontSize: "35px", color: "#0DCAF0"}}/>
                    </div>

                    <div className="name">{user?.email}</div>
                </div>

                <ul className="menu-box">
                    {
                        sidebarData.map((item, index) => (
                            <li className="menu-item">

                                <NavLink to={item.pathname} activeClassName="active-link">
                                    <div className="icon-box">
                                        {item.icon}
                                    </div>
                                    <div className="title fw-bold">
                                        {item.title}
                                    </div>
                                </NavLink>

                            </li>
                        ))
                    }

                    <li className="menu-item">

                        <a className="d-flex mb-2" href="" onClick={()=>LogOutFunction()}>
                            <div className="icon-box">
                                <LogoutIcon/>
                            </div>
                            <div className="title fw-bold">
                                Logout
                            </div>
                        </a>

                    </li>

                </ul>


            </div>

            <div className="content">
                {children}
            </div>

        </div>
    );
};

export default Layout;