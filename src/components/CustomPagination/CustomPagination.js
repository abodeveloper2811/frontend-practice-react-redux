import React from 'react';
import Pagination from "@mui/material/Pagination/Pagination";
import "./Pagination.scss";

const CustomPagination = ({current_page, setCurrentPage, numberOfPages}) => {

    const handleChangePage = (event, newPage) => {
        setCurrentPage(newPage);
    };

    return (
        <div className="d-flex align-items-center justify-content-center my-5">
            <Pagination
                count={numberOfPages}
                variant="outlined"
                shape="rounded"
                page={current_page}
                onChange={handleChangePage}
            />
        </div>
    );
};

export default CustomPagination;