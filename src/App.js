import './App.css';
import {Redirect, Route, Switch} from "react-router-dom";
import React, {useEffect, useState} from "react";
import SiteLoader from "./components/SiteLoader/SiteLoader";
import Home from "./pages/Home/Home";
import {ToastContainer} from "react-toastify";
import ConnectCounter from "./pages/Connect-counter/ConnectCounter";
import HookCounter from "./pages/Hook-counter/HookCounter";
import StudentsCrud from "./pages/Students-CRUD/StudentsCrud";
import ProductsCrud from "./pages/Products-CRUD/ProductsCrud";
import ProductPage from "./pages/Products-CRUD/ProductPage";
import SignInPage from "./pages/AuthPage/SignInPage";
import SignUpPage from "./pages/AuthPage/SignUpPage";
import PrivateRoute from "./routes/PrivateRoute";
import Blogs from "./pages/Blogs/Blogs";
import SendMessageToTelegram from "./pages/SendMessageToTelegram/SendMessageToTelegram";
import AnimationsWithAos from "./pages/AninationWithAos/AnimationsWithAos";
import AnimationWithReveal from "./pages/AnimationWithReveal/AnimationWithReveal";

function App() {

    const [site_loading, setSiteLoading] = useState(true);

    useEffect(() => {

        setTimeout(() => {
            setSiteLoading(false);
        }, 1000);

    }, []);

    if (site_loading) {
        return (
            <SiteLoader/>
        )
    } else {

        return (
            <>
                <div className="App">

                    <Switch>

                        <Route exact path="/sign-in" component={SignInPage}/>
                        <Route exact path="/sign-up" component={SignUpPage}/>

                        <PrivateRoute exact path="/home" component={Home}/>
                        <PrivateRoute exact path="/connect-counter" component={ConnectCounter}/>
                        <PrivateRoute exact path="/hook-counter" component={HookCounter}/>
                        <PrivateRoute exact path="/students-crud" component={StudentsCrud}/>
                        <PrivateRoute exact path="/products-crud" component={ProductsCrud}/>
                        <PrivateRoute exact path="/products-crud/:id" component={ProductPage}/>
                        <PrivateRoute exact path="/blogs-crud" component={Blogs}/>
                        <PrivateRoute exact path="/send-message-to-telegram" component={SendMessageToTelegram}/>
                        <PrivateRoute exact path="/animations-with-aos" component={AnimationsWithAos}/>
                        <PrivateRoute exact path="/animations-with-reveal" component={AnimationWithReveal}/>

                        <Route exact path="/">
                            <Redirect to="/home"/>
                        </Route>

                    </Switch>

                </div>

                <ToastContainer autoClose={2000}/>
            </>
        );
    }

}

export default App;
