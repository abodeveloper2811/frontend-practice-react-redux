import React from 'react';
import Layout from "../../components/Layout/Layout";
import Fade from 'react-reveal/Fade';
import Flip from 'react-reveal/Flip';
import Rotate from 'react-reveal/Rotate';
import Zoom from 'react-reveal/Zoom';
import Bounce from 'react-reveal/Bounce';
import Slide from 'react-reveal/Slide';
import Roll from 'react-reveal/Roll';


const AnimationWithReveal = () => {
    return (
        <Layout>

            <Fade top>
                <h3 className="text-info my-text fw-bold mb-5">Animation with react-reveal</h3>
            </Fade>

            <div className="row">
                <div className="col-md-4">
                    <Flip top duration={3000} delay={2000}>
                        <div className="card">
                            <div className="card-header">
                                My CARD
                            </div>
                            <div className="card-body">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum eaque est minus officiis
                                quam quas rem, veniam voluptate? Assumenda beatae cumque, debitis dicta dignissimos
                                distinctio esse ex facere facilis harum id inventore ipsam nam nemo neque nobis nulla
                                perspiciatis placeat quia rem saepe soluta sunt ut? Aliquam deserunt fuga ipsa?
                            </div>
                        </div>
                    </Flip>
                </div>
                <div className="col-md-4">
                    <Rotate bottom right>
                        <div className="card">
                            <div className="card-header">
                                My CARD
                            </div>
                            <div className="card-body">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum eaque est minus officiis
                                quam quas rem, veniam voluptate? Assumenda beatae cumque, debitis dicta dignissimos
                                distinctio esse ex facere facilis harum id inventore ipsam nam nemo neque nobis nulla
                                perspiciatis placeat quia rem saepe soluta sunt ut? Aliquam deserunt fuga ipsa?
                            </div>
                        </div>
                    </Rotate>
                </div>
                <div className="col-md-4">
                    <Zoom right>
                        <div className="card">
                            <div className="card-header">
                                My CARD
                            </div>
                            <div className="card-body">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum eaque est minus officiis
                                quam quas rem, veniam voluptate? Assumenda beatae cumque, debitis dicta dignissimos
                                distinctio esse ex facere facilis harum id inventore ipsam nam nemo neque nobis nulla
                                perspiciatis placeat quia rem saepe soluta sunt ut? Aliquam deserunt fuga ipsa?
                            </div>
                        </div>
                    </Zoom>
                </div>
            </div>

            <div className="row">
                <div className="col-md-4">
                    <Bounce left>
                        <div className="card">
                            <div className="card-header">
                                My CARD
                            </div>
                            <div className="card-body">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum eaque est minus officiis
                                quam quas rem, veniam voluptate? Assumenda beatae cumque, debitis dicta dignissimos
                                distinctio esse ex facere facilis harum id inventore ipsam nam nemo neque nobis nulla
                                perspiciatis placeat quia rem saepe soluta sunt ut? Aliquam deserunt fuga ipsa?
                            </div>
                        </div>
                    </Bounce>
                </div>
                <div className="col-md-4">
                    <Slide bottom>
                        <div className="card">
                            <div className="card-header">
                                My CARD
                            </div>
                            <div className="card-body">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum eaque est minus officiis
                                quam quas rem, veniam voluptate? Assumenda beatae cumque, debitis dicta dignissimos
                                distinctio esse ex facere facilis harum id inventore ipsam nam nemo neque nobis nulla
                                perspiciatis placeat quia rem saepe soluta sunt ut? Aliquam deserunt fuga ipsa?
                            </div>
                        </div>
                    </Slide>
                </div>
                <div className="col-md-4">
                    <Roll right>
                        <div className="card">
                            <div className="card-header">
                                My CARD
                            </div>
                            <div className="card-body">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum eaque est minus officiis
                                quam quas rem, veniam voluptate? Assumenda beatae cumque, debitis dicta dignissimos
                                distinctio esse ex facere facilis harum id inventore ipsam nam nemo neque nobis nulla
                                perspiciatis placeat quia rem saepe soluta sunt ut? Aliquam deserunt fuga ipsa?
                            </div>
                        </div>
                    </Roll>
                </div>
            </div>

        </Layout>
    );
};

export default AnimationWithReveal;