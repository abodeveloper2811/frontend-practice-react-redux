import React, {useEffect, useState} from 'react';
import Layout from "../../components/Layout/Layout";
import ModeEditIcon from '@mui/icons-material/ModeEdit';
import DeleteForeverIcon from '@mui/icons-material/DeleteForever';
import GroupAddIcon from '@mui/icons-material/GroupAdd';
import {useDispatch, useSelector} from "react-redux";
import AddStudent from "../../components/Students-CRUD/AddStudent";
import DeleteStudent from "../../components/Students-CRUD/DeleteStudent";
import {setDeleteStudentId, setUpdateStudentId} from "../../redux/actions/studentsActions";
import EditStudent from "../../components/Students-CRUD/EditStudent";
import CustomPagination from "../../components/CustomPagination/CustomPagination";

const StudentsCrud = () => {

    const dispatch = useDispatch();

    const [search, setSearch] = useState('');
    const [current_page, setCurrentPage] = useState(1);
    const [numberOfPages, setNumberOfPages] = useState(1);
    const [renderData, setRenderData] = useState([]);

    const {students} = useSelector(state => state.students);

    const [open_add_modal, setOpenAddModal] = useState(false);

    const toggle = () => setOpenAddModal(!open_add_modal);

    const openAddModalFunction = () => setOpenAddModal(true);

    const closeAddModalFunction = () => setOpenAddModal(false);

    const setDeleteStudent = (id) => {
        dispatch(setDeleteStudentId(id));
    };

    const setUpdateStudent = (id) => {
        dispatch(setUpdateStudentId(id));
    };

    const onFilterHandler = (search, arr) => {

        if (search.length === 0) {
            return arr;
        }

        let search_text = search.toLowerCase().trim();

        return arr.filter(item =>
            item.firstName.toLowerCase().includes(search_text) ||
            item.lastName.toLowerCase().includes(search_text) ||
            item.status.toLowerCase().includes(search_text) ||
            String(item.age).includes(search_text)
        );
    };

    const onPaginationHandler = (arr, page) => {
        let pages = Math.floor((arr.length + 5 - 1) / 5);
        setNumberOfPages(pages);
        setRenderData(arr.slice((page - 1) * 5, page * 5));
    };

    useEffect(() => {
        onPaginationHandler(onFilterHandler(search, students), current_page)
    }, [current_page, search, students]);

    return (
        <Layout>

            <div className="Students">

                <div className="row mb-4">
                    <div className="col-md-12">
                        <div className="d-flex align-items-center justify-content-between">
                            <h1 className="text-info">Talabalar</h1>

                            <div className="w-25 input-box">
                                <input
                                    value={search}
                                    type="search"
                                    className="form-control"
                                    placeholder="Qidirish"
                                    onChange={(event) => {
                                        setCurrentPage(1);
                                        setSearch(event.target.value)
                                    }}
                                />
                            </div>

                            <button className="btn btn-success">
                                <span className="me-2" onClick={() => openAddModalFunction()}>Talaba qo'shish</span>
                                <GroupAddIcon/>
                            </button>
                        </div>
                    </div>
                </div>

                <div className="table-responsive">
                    <table className="table table-bordered text-dark text-center">
                        <thead style={{backgroundColor: "#0DCAF0", color: "white"}}>
                        <tr>
                            <th>№</th>
                            <th>Ismi</th>
                            <th>Familiyasi</th>
                            <th>Yoshi</th>
                            <th>O'qish turi</th>
                            <th>Amallar</th>
                        </tr>
                        </thead>

                        <tbody>
                        {
                            renderData?.map((student, index) => {
                                return (
                                    <tr key={index}>
                                        <td>{(current_page - 1)* 5 + index+1}</td>
                                        <td>{student?.firstName}</td>
                                        <td>{student?.lastName}</td>
                                        <td>{student?.age}</td>
                                        <td>
                                            <div
                                                className={`${student?.status === "Grand" ? "bg-success text-white fw-bold" : "text-white fw-bold bg-warning"}`}>{student?.status}</div>
                                        </td>
                                        <td>
                                            <td className="d-flex align-items-center justify-content-evenly">
                                                <button className="btn-sm btn-danger">
                                                    <DeleteForeverIcon onClick={() => setDeleteStudent(student?.id)}/>
                                                </button>
                                                <button className="btn-sm btn-warning mt-1">
                                                    <ModeEditIcon onClick={() => setUpdateStudent(student?.id)}/>
                                                </button>
                                            </td>
                                        </td>
                                    </tr>
                                )
                            })
                        }
                        </tbody>

                    </table>
                </div>

                <CustomPagination numberOfPages={numberOfPages} current_page={current_page} setCurrentPage={setCurrentPage}/>

            </div>

            <AddStudent
                open_add_modal={open_add_modal}
                toggle={toggle}
                openAddModalFunction={openAddModalFunction}
                closeAddModalFunction={closeAddModalFunction}
            />

            <DeleteStudent/>

            <EditStudent/>

        </Layout>
    );
};

export default StudentsCrud;