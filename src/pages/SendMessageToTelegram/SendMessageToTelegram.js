import React from 'react';
import Layout from "../../components/Layout/Layout";
import {AvField, AvForm} from "availity-reactstrap-validation";
import axios from "axios";
import {toast} from "react-toastify";

const SendMessageToTelegram = () => {

    const SendMessageFunction = (event, data) => {

        const TOKEN = "6297477304:AAHe0DFlJxF95jgUJRKv6YqRmPMGca4fvoI";
        const CHAT_ID = "-1001756971158";
        const API_URL = `https://api.telegram.org/bot${TOKEN}/sendMessage`;

        let message = `
            ✅ Заказ от сайта:
         👤 Имя:  ${data.name}
         📞 Номер телефона :  ${data.phone}
         📝 Описание:  ${data.description}
        `;

        axios.post(API_URL, {
            chat_id: CHAT_ID,
            parse_mode: 'html',
            text: message,
        })

            .then((res) => {
                toast.success("Xabar muvaffaqiyatli yuborildi !!!");
                event.target.name.value="";
                event.target.phone.value="";
                event.target.description.value="";
            }).catch((err) => {
            toast.error("Xabar yuborilmadi !!!");
        })

    };

    return (
        <Layout>

            <div className="row">
                <div className="col-md-6">
                    <div className="form-box">

                        <h4 className="fw-bold text-info mb-3">Biz bilan bog'laning</h4>

                        <AvForm onValidSubmit={SendMessageFunction}>

                            <AvField
                                type="text"
                                label="Ismingiz"
                                name="name"
                                className="form-control"
                                required
                                validate={{
                                    required: {value: true, errorMessage: 'Ismingizni kiriting !'}
                                }}
                            />

                            <div className="mb-3"/>

                            <AvField
                                type="text"
                                label="Telefon"
                                name="phone"
                                className="form-control"
                                required
                                validate={{
                                    required: {value: true, errorMessage: 'Telefon raqamingizni kiriting !'}
                                }}
                            />

                            <div className="mb-3"/>

                            <AvField
                                type="textarea"
                                label="Izoh"
                                name="description"
                                className="form-control"
                                style={{height: "150px"}}
                            />

                            <div className="mb-3"/>

                            <button type="submit" className="ps-4 pe-4 btn-info btn text-white fw-bold">Yuborish
                            </button>

                        </AvForm>

                    </div>
                </div>
            </div>
        </Layout>
    );
};

export default SendMessageToTelegram;