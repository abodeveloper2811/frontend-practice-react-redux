import React from 'react';
import Layout from "../../components/Layout/Layout";
import {useHistory} from "react-router-dom";
import IsoIcon from '@mui/icons-material/Iso';
import ExposureIcon from '@mui/icons-material/Exposure';
import SchoolIcon from '@mui/icons-material/School';
import ShoppingCartIcon from '@mui/icons-material/ShoppingCart';
import GroupsIcon from '@mui/icons-material/Groups';
import PostAddIcon from '@mui/icons-material/PostAdd';
import MarkEmailReadIcon from '@mui/icons-material/MarkEmailRead';
import "./home.scss";

const Home = () => {

    const history = useHistory();

    const data = [
        {
            icon: <IsoIcon/>,
            title: "Counter with Connect",
            pathname: '/connect-counter'
        },
        {
            icon: <ExposureIcon/>,
            title: "Counter with Hook",
            pathname: '/hook-counter'
        },
        {
            icon: <SchoolIcon/>,
            title: "Students CRUD with localhost",
            pathname: '/students-crud'
        },
        {
            icon: <ShoppingCartIcon/>,
            title: "Products CRUD with json-server",
            pathname: '/products-crud'
        },
        {
            icon: <PostAddIcon/>,
            title: "Blogs CRUD with JWT TOKEN",
            pathname: '/blogs-crud'
        },
        {
            icon: <MarkEmailReadIcon/>,
            title: "Send message to Telegram API",
            pathname: '/send-message-to-telegram'
        }
    ];

    return (
        <Layout>
            <div className="Home">

                <h1 className="text-info mb-4">Dashboard</h1>

                <div className="row">
                    {
                        data.map((item, index)=>(
                            <div className="col-md-4" key={index}>
                                <div className="section-card" onClick={()=>history.push(`${item.pathname}`)}>
                                    <div className="title">{item.title}</div>
                                    <div className="icon-box">
                                        <div className="icon">{item.icon}</div>
                                    </div>
                                </div>
                            </div>
                        ))
                    }

                </div>

            </div>
        </Layout>
    );
};

export default Home;