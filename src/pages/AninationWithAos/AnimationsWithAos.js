import React from 'react';
import Layout from "../../components/Layout/Layout";
import AOS from 'aos';
import 'aos/dist/aos.css';

const AnimationsWithAos = () => {

    return (
        <Layout>
            <h1 className="text-info fw-bold">Animation with AOS</h1>

            <div className="row mt-5">

                <div className="col-md-4">
                    <div data-aos="fade-down-left">

                        <div className="card">
                            <div className="card-header">
                                My CARD
                            </div>
                            <div className="card-body">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum eaque est minus officiis
                                quam quas rem, veniam voluptate? Assumenda beatae cumque, debitis dicta dignissimos
                                distinctio esse ex facere facilis harum id inventore ipsam nam nemo neque nobis nulla
                                perspiciatis placeat quia rem saepe soluta sunt ut? Aliquam deserunt fuga ipsa?
                            </div>
                        </div>

                    </div>
                    <div data-aos="fade-up">

                        <div className="card">
                            <div className="card-header">
                                My CARD
                            </div>
                            <div className="card-body">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum eaque est minus officiis
                                quam quas rem, veniam voluptate? Assumenda beatae cumque, debitis dicta dignissimos
                                distinctio esse ex facere facilis harum id inventore ipsam nam nemo neque nobis nulla
                                perspiciatis placeat quia rem saepe soluta sunt ut? Aliquam deserunt fuga ipsa?
                            </div>
                        </div>

                    </div>
                    <div data-aos="fade-up">

                        <div className="card">
                            <div className="card-header">
                                My CARD
                            </div>
                            <div className="card-body">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum eaque est minus officiis
                                quam quas rem, veniam voluptate? Assumenda beatae cumque, debitis dicta dignissimos
                                distinctio esse ex facere facilis harum id inventore ipsam nam nemo neque nobis nulla
                                perspiciatis placeat quia rem saepe soluta sunt ut? Aliquam deserunt fuga ipsa?
                            </div>
                        </div>

                    </div>
                    <div data-aos="fade-up">

                        <div className="card">
                            <div className="card-header">
                                My CARD
                            </div>
                            <div className="card-body">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum eaque est minus officiis
                                quam quas rem, veniam voluptate? Assumenda beatae cumque, debitis dicta dignissimos
                                distinctio esse ex facere facilis harum id inventore ipsam nam nemo neque nobis nulla
                                perspiciatis placeat quia rem saepe soluta sunt ut? Aliquam deserunt fuga ipsa?
                            </div>
                        </div>

                    </div>
                    <div data-aos="fade-up">

                        <div className="card">
                            <div className="card-header">
                                My CARD
                            </div>
                            <div className="card-body">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum eaque est minus officiis
                                quam quas rem, veniam voluptate? Assumenda beatae cumque, debitis dicta dignissimos
                                distinctio esse ex facere facilis harum id inventore ipsam nam nemo neque nobis nulla
                                perspiciatis placeat quia rem saepe soluta sunt ut? Aliquam deserunt fuga ipsa?
                            </div>
                        </div>

                    </div>
                </div>

                <div className="col-md-4">
                    <div data-aos="fade-up">

                        <div className="card">
                            <div className="card-header">
                                My CARD
                            </div>
                            <div className="card-body">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum eaque est minus officiis
                                quam quas rem, veniam voluptate? Assumenda beatae cumque, debitis dicta dignissimos
                                distinctio esse ex facere facilis harum id inventore ipsam nam nemo neque nobis nulla
                                perspiciatis placeat quia rem saepe soluta sunt ut? Aliquam deserunt fuga ipsa?
                            </div>
                        </div>

                    </div>
                </div>

                <div className="col-md-4">
                    <div data-aos="fade-down">

                        <div className="card">
                            <div className="card-header">
                                My CARD
                            </div>
                            <div className="card-body">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum eaque est minus officiis
                                quam quas rem, veniam voluptate? Assumenda beatae cumque, debitis dicta dignissimos
                                distinctio esse ex facere facilis harum id inventore ipsam nam nemo neque nobis nulla
                                perspiciatis placeat quia rem saepe soluta sunt ut? Aliquam deserunt fuga ipsa?
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </Layout>
    );
};

export default AnimationsWithAos;