import React, {useEffect, useState} from 'react';
import Layout from "../../components/Layout/Layout";
import {connect} from 'react-redux';
import {Decrement, Increment, Reset} from "../../redux/actions/counterActions";
import MyLogo from "../../images/Avatar.jpg";
import { LazyLoadImage } from 'react-lazy-load-image-component';
import 'react-lazy-load-image-component/src/effects/blur.css';
import 'react-lazy-load-image-component/src/effects/black-and-white.css';
import 'react-lazy-load-image-component/src/effects/opacity.css';

const ConnectCounter = ({count, increment, decrement, reset}) => {

    const [step, setStep] = useState(0);

    const Increment =()=>{
      increment(step);
    };

    const Decrement =()=>{
        decrement(step);
    };

    const ResetCounter =()=>{
        reset();
        setStep(0);
    };

    const changeStep =(e)=>{
        setStep(Number.parseInt(e.target.value));
    };

    return (
        <Layout>
            <div className="connect-counter">
                <div className="container">
                    <div className="row">
                        <div className="col-md-4">
                            <div className="card w-100">

                                <div className="card-header bg-info">
                                    <h3 className="text-center text-white">Connect Counter</h3>
                                </div>

                                <div className="card-body">
                                    <h4 className="text-center my-4">{count}</h4>

                                    <div className="step d-flex align-items-center justify-content-between">

                                        <div>Step: </div>

                                        <input
                                            type="number"
                                            className="form-control ms-2"
                                            min={0}
                                            value={step}
                                            onChange={changeStep}
                                        />

                                    </div>

                                    <div className="d-flex justify-content-around mt-4">
                                        <button className="btn btn-danger" onClick={()=>Decrement()}>-</button>
                                        <button className="btn btn-info text-white" onClick={()=>ResetCounter()}>RESET</button>
                                        <button className="btn btn-success" onClick={()=>Increment()}>+</button>
                                    </div>

                                </div>
                            </div>
                        </div>

                        <div className="col-md-4">
                            <img src={MyLogo} alt=""/>

                            <LazyLoadImage
                                alt={"Bu alternativ matn"}
                                effect="blur"
                                delayTime="25000"
                                delayMethod="debounce"
                                src={MyLogo} />

                        </div>
                    </div>


                </div>

            </div>
        </Layout>
    );
};

function mapStateToProps(state) {

    const { counter } = state;

    return {
        count:  counter.count,
        loading: counter.loading
    }
}


const mapDispatchToProps = (dispatch) => {

    return {
        increment: (step) => dispatch(Increment(step)),
        decrement: (step) => dispatch(Decrement(step)),
        reset: () => dispatch(Reset()),
    }

};

export default connect(mapStateToProps, mapDispatchToProps)(ConnectCounter);