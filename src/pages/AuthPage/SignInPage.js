import React from 'react';
import {AvField, AvForm} from "availity-reactstrap-validation";
import {Link} from "react-router-dom";
import {BsPersonCircle} from "react-icons/bs";
import {useDispatch, useSelector} from "react-redux";
import {SignIn} from "../../redux/actions/authActions";
import {useHistory} from 'react-router-dom';

const SignInPage = () => {

    const dispatch = useDispatch();
    const history = useHistory();

    const {btn_loading} = useSelector(state => state.auth);

    const SignInFunction = (event, userData)=>{
        dispatch(SignIn(userData, history));
    };

    const myStyle = {
        backgroundColor: '#EBEBFF'
    };

    return (
        <div className="My-login" style={myStyle}>
            <div className="container vh-100 d-flex flex-column justify-content-center">
                <div className="row">
                    <div className="col-md-6 h-100 mx-auto">
                        <div className="d-flex w-100 flex-column align-items-center justify-content-center">
                            <h3 className="title font-weight-bold text-center">
                                Tizimga kirish
                            </h3>

                            <p className="commit text-center">
                                Email va parolingizni kiriting !!!
                            </p>

                            <div className="card w-100 p-2">
                                <div className="card-body d-flex flex-column align-items-center justify-content-center">
                                    <div className="text-center">
                                        <BsPersonCircle
                                            style={{fontSize: "70px", color: " #0DCAF0"}}
                                        />
                                    </div>

                                    <AvForm className="w-100" onValidSubmit={SignInFunction}>

                                        <AvField
                                            className="form-control"
                                            label="Email"
                                            type="email"
                                            name="email"
                                            required
                                            validate={{
                                                required: {value: true, errorMessage: 'Email ni kiriting !'}
                                            }}
                                        />

                                        <div className="mb-3"/>

                                        <div className="input-box">
                                            <AvField
                                                className="form-control"
                                                label="Parol"
                                                type="password"
                                                name="password"
                                                required
                                                validate={{
                                                    required: {value: true, errorMessage: 'Parol ni kiriting !'}
                                                }}
                                            />
                                        </div>

                                        <div className="mb-3"/>

                                        <div className="text-center">
                                            <button
                                                disabled={btn_loading}
                                                type="submit"
                                                className="btn mt-2 text-white fw-bold"
                                                style={{backgroundColor: '#0DCAF0'}}
                                            >
                                                Tizimga kirish
                                            </button>
                                        </div>

                                        <h6 className="d-flex align-items-center justify-content-center sign-up-box text-center mt-3">
                                            <div className="sign-up-title me-3">Ro'yxatdan o'tmaganmisiz ?</div>
                                            <Link to="/sign-up">Ro'yxatdan o'tish</Link>
                                        </h6>

                                    </AvForm>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default SignInPage;