import React from 'react';
import {AvField, AvForm} from "availity-reactstrap-validation";
import {Link} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import {SignUp} from "../../redux/actions/authActions";
import {useHistory} from 'react-router-dom';

const SignUpPage = () => {

    const dispatch = useDispatch();
    const history = useHistory();

    const {btn_loading} = useSelector(state => state.auth);

    const SignUpFunction =(event, newUserData)=>{
        dispatch(SignUp(newUserData, history))
    };

    const myStyle = {
        backgroundColor: '#EBEBFF'
    };

    return(
        <div className="Auth" style={myStyle}>
            <div className="container vh-100 d-flex flex-column justify-content-center">
                <div className="row">
                    <div className="col-md-6 h-100 mx-auto">
                        <div className="d-flex w-100 flex-column align-items-center justify-content-center">
                            <h3 className="title font-weight-bold text-center">
                                Ro'yxatdan o'tish
                            </h3>
                            <p className="commit text-center">
                                Ro'yxatdan o'tish uchun kerakli ma'lumotlarni kiriting !!!
                            </p>

                            <div className="card w-100 p-2">
                                <div className="card-body d-flex flex-column align-items-center justify-content-center">

                                    <AvForm className="w-100" onValidSubmit={SignUpFunction}>

                                        <AvField
                                            className="form-control mb-3"
                                            label="Email"
                                            type="email"
                                            name="email"
                                            required
                                            validate={{
                                                required: {value: true, errorMessage: 'Email ni kiriting !'}
                                            }}
                                        />

                                        <div className="mb-3"/>

                                        <AvField
                                            className="form-control"
                                            label="Foydalanuvchi nomi"
                                            type="text"
                                            name="name"
                                            required
                                            validate={{
                                                required: {value: true, errorMessage: 'Username ni kiriting !'}
                                            }}
                                        />

                                        <div className="mb-3"/>

                                        <AvField
                                            className="form-control"
                                            label="Parol"
                                            type="password"
                                            name="password"
                                            required
                                            validate={{
                                                required: {value: true, errorMessage: 'Parol ni kiriting !'}
                                            }}
                                        />

                                        <div className="mb-3"/>

                                        <AvField
                                            className="form-control"
                                            label="Parolni qayta kiriting"
                                            type="password"
                                            name="password2"
                                            required
                                            validate={{
                                                required: {value: true, errorMessage: 'Parol ni kiriting !'}
                                            }}
                                        />

                                        <div className="mb-3"/>

                                        <AvField
                                            type="select"
                                            label="Jinsi"
                                            name="tc"
                                            className="form-select"
                                            required
                                            validate={{
                                                required: {value: true, errorMessage: "Jins ni tanlang !"}
                                            }}
                                        >
                                            <option value="" selected={true} disabled={true}>Tanlang</option>
                                            <option value="true">Erkak</option>
                                            <option value="false">Ayol</option>
                                        </AvField>

                                        <div className="mb-3"/>

                                        <div className="text-center">
                                            <button
                                                disabled={btn_loading}
                                                type="submit"
                                                className="btn mt-2 text-white fw-bold"
                                                style={{backgroundColor: '#0DCAF0'}}
                                            >Ro'yxatdan o'tish</button>
                                        </div>

                                        <h6 className="d-flex align-items-center justify-content-center sign-up-box text-center mt-3">
                                            <div className="sign-up-title me-3">Ro'yxatdan o'tganmisiz ?</div>
                                            <Link to="/sign-in">Tizimga kirish</Link>
                                        </h6>

                                    </AvForm>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );

};

export default SignUpPage;