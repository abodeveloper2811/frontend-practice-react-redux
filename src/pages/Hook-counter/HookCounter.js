import React, {useState} from 'react';
import Layout from "../../components/Layout/Layout";
import {useSelector, useDispatch} from "react-redux";
import {Decrement, Increment, Reset} from "../../redux/actions/counterActions";

const HookCounter = () => {

    const [step, setStep] = useState(0);

    const { count } = useSelector(state => state.counter);

    const dispatch = useDispatch();

    const InrementCounter = ()=>{
        dispatch(Increment(step));
    };

    const DecrementCounter = ()=>{
        dispatch(Decrement(step));
    };

    const ResetCounter = ()=>{
        dispatch(Reset());
        setStep(0);
    };

    const ChangeStep =(step)=>{
        setStep(Number.parseInt(step));
    };

    return (
        <Layout>
            <div className="connect-counter">
                <div className="container">
                    <div className="row">
                        <div className="col-md-4">
                            <div className="card w-100">

                                <div className="card-header bg-info">
                                    <h3 className="text-center text-white">Hook Counter</h3>
                                </div>

                                <div className="card-body">
                                    <h4 className="text-center my-4">{count}</h4>


                                    <div className="step d-flex align-items-center justify-content-between">
                                        <div>Step: </div>

                                        <input
                                            type="number"
                                            className="form-control ms-2"
                                            value={step}
                                            onChange={(e)=>ChangeStep(e.target.value)}
                                        />
                                    </div>

                                    <div className="d-flex justify-content-around mt-4">
                                        <button className="btn btn-danger" onClick={()=>DecrementCounter()}>-</button>
                                        <button className="btn btn-info text-white" onClick={()=>ResetCounter()}>RESET</button>
                                        <button className="btn btn-success" onClick={()=>InrementCounter()}>+</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </Layout>
    );
};

export default HookCounter;