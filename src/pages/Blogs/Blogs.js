import React, {useEffect, useState} from 'react';
import Layout from "../../components/Layout/Layout";
import GroupAddIcon from '@mui/icons-material/GroupAdd';
import {useDispatch, useSelector} from "react-redux";
import {getBlogs} from "../../redux/actions/blogsActions";
import VisibilityIcon from "@mui/icons-material/Visibility";
import ModeEditIcon from '@mui/icons-material/ModeEdit';
import DeleteForeverIcon from '@mui/icons-material/DeleteForever';
import CustomPagination from "../../components/CustomPagination/CustomPagination";
import TableLoading from "../../components/TableLoading/TableLoading";
import CreateBlog from "../../components/Blogs-CRUD/CreateBlog";
import {SET_DELETE_BLOG, SET_UPDATE_BLOG} from "../../redux/actionTypes/actionsTypes";
import UpdateBlog from "../../components/Blogs-CRUD/UpdateBlog";
import DeleteBlog from "../../components/Blogs-CRUD/DeleteBlog";

const Blogs = () => {

    const [current_page, setCurrentPage] = useState(1);

    const [show_add_modal, setShowAddModal] = useState(false);

    const openAddModalFunction =()=> setShowAddModal(true);
    const closeAddModalFunction =()=> setShowAddModal(false);

    const dispatch = useDispatch();

    const {blogs, loading, numberOfPages} = useSelector(state => state.blogs);

    useEffect(()=>{
        dispatch(getBlogs(current_page));
    },[current_page]);

    const setUpdateBlogFunction =(blog)=>{
        dispatch({
            type: SET_UPDATE_BLOG,
            payload: blog
        })
    };

    const setDeleteBlogFunction =(blog)=>{
        dispatch({
            type: SET_DELETE_BLOG,
            payload: blog
        })
    };

    return (
        <Layout>

            <div className="Blogs">

                <div className="row mb-4">
                    <div className="col-md-12">
                        <div className="d-flex align-items-center justify-content-between">
                            <h1 className="text-info">Bloglar</h1>

                            <button className="btn btn-success" onClick={()=>openAddModalFunction()}>
                                <span className="me-2">Blog yaratish</span>
                                <GroupAddIcon/>
                            </button>
                        </div>
                    </div>
                </div>

                <div className="table-responsive">
                    <table className="table table-bordered text-dark text-center">
                        <thead style={{backgroundColor: "#0DCAF0", color: "white"}}>
                        <tr>
                            <th className="col-1">№</th>
                            <th className="col-3">Rasm</th>
                            <th className="col-3">Sarlavha</th>
                            <th className="col-6">Tavsif</th>
                            <th className="col-2">Amallar</th>
                        </tr>
                        </thead>

                        <tbody>
                        {
                            !loading ?

                            blogs?.map((blog, index)=>(
                                <tr>
                                    <td>
                                        {(current_page - 1) * 5 + index + 1}
                                    </td>
                                    <td>
                                        <img width="200px" src={blog?.thumbnail} alt=""/>
                                    </td>
                                    <td>
                                        <b>{blog.title}</b>
                                    </td>
                                    <td>
                                        {blog.description}
                                    </td>
                                    <td className="d-flex align-items-center justify-content-evenly">
                                        <button className="btn-sm btn-danger" onClick={()=>setDeleteBlogFunction(blog)}>
                                            <DeleteForeverIcon/>
                                        </button>
                                        <button className="btn-sm btn-warning mt-1" onClick={()=>setUpdateBlogFunction(blog)}>
                                            <ModeEditIcon/>
                                        </button>
                                    </td>
                                </tr>
                            )) :

                                <tr>
                                    <td colSpan={4}>
                                        <TableLoading/>
                                    </td>
                                </tr>
                        }
                        </tbody>

                    </table>
                </div>

                { blogs.length > 0  ?

                    <CustomPagination numberOfPages={numberOfPages} current_page={current_page} setCurrentPage={setCurrentPage}/> : ""
                }

            </div>

            <CreateBlog setCurrentPage={setCurrentPage} show_add_modal={show_add_modal} closeAddModalFunction={closeAddModalFunction}/>

            <UpdateBlog setCurrentPage={setCurrentPage}/>

            <DeleteBlog setCurrentPage={setCurrentPage}/>

        </Layout>
    );
};

export default Blogs;