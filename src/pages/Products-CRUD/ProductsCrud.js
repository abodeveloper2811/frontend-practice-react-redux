import React, {useEffect, useState} from 'react';
import Layout from "../../components/Layout/Layout";
import GroupAddIcon from '@mui/icons-material/GroupAdd';
import ModeEditIcon from '@mui/icons-material/ModeEdit';
import DeleteForeverIcon from '@mui/icons-material/DeleteForever';
import VisibilityIcon from '@mui/icons-material/Visibility';
import {useDispatch, useSelector} from "react-redux";
import {getProducts, setDeleteProduct, setUpdateProduct} from "../../redux/actions/productsActions";
import CustomPagination from "../../components/CustomPagination/CustomPagination";
import CreateProduct from "../../components/Products-CRUD/CreateProduct";
import DeleteProduct from "../../components/Products-CRUD/DeleteProduct";
import UpdateProduct from "../../components/Products-CRUD/UpdateProduct";
import TableLoading from "../../components/TableLoading/TableLoading";
import {useHistory} from 'react-router-dom';
import CurrencyFormat from "react-currency-format";

const ProductsCrud = () => {

    const history = useHistory();

    const [search_text, setSearchText] = useState('');
    const [current_page, setCurrentPage] = useState(1);
    const [limit, setLimit] = useState(4);

    const [add_open_modal, setAddOpenModal] = useState(false);
    const toggle = () => setAddOpenModal(!add_open_modal);

    const openAddModalFunction = () => setAddOpenModal(true);
    const closeAddModalFunction = () => setAddOpenModal(false);

    const dispatch = useDispatch();

    const {products, numberOfPages, loading} = useSelector(state => state.products);

    useEffect(() => {
        dispatch(getProducts(search_text, limit, current_page));
    }, [current_page, search_text]);

    const setDeleteProductFunction = (delete_product) => {
        dispatch(setDeleteProduct(delete_product))
    };

    const setUpdateProductFunction = (update_product) => {
        dispatch(setUpdateProduct(update_product))
    };

    return (
        <Layout>

            <div className="Products">

                <div className="row mb-4">
                    <div className="col-md-12">
                        <div className="d-flex align-items-center justify-content-between">
                            <h1 className="text-info">Mahsulotlar</h1>

                            <div className="w-25 input-box">
                                <input
                                    value={search_text}
                                    type="search"
                                    className="form-control"
                                    placeholder="Qidirish"
                                    onChange={(e) => {
                                        setSearchText(e.target.value);
                                        setCurrentPage(1);
                                    }}
                                />
                            </div>

                            <button className="btn btn-success" onClick={openAddModalFunction}>
                                <span className="me-2">Mahsulot qo'shish</span>
                                <GroupAddIcon/>
                            </button>
                        </div>
                    </div>
                </div>

                <div className="table-responsive">
                    <table className="table table-bordered text-dark text-center">
                        <thead style={{backgroundColor: "#0DCAF0", color: "white"}}>
                        <tr>
                            <th>№</th>
                            <th>Mahsulot nomi</th>
                            <th>Mahsulot formati</th>
                            <th>Narxi</th>
                            <th>Amallar</th>
                        </tr>
                        </thead>

                        <tbody>

                        {

                            loading ?

                                <tr>
                                    <td colSpan={5}>
                                        <TableLoading/>
                                    </td>
                                </tr>

                                :
                                    products.length !== 0 ?

                                    products.map((product, index) => (

                                    <tr key={product.id}>
                                        <td>{(current_page - 1) * limit + index + 1}</td>
                                        <td>{product.name}</td>
                                        <td>{product.format}</td>
                                        <td>
                                            <CurrencyFormat
                                                thousandSpacing="3"
                                                displayType="text"
                                                value={product.price}
                                                thousandSeparator={' '}
                                                suffix={" so'm"}
                                            />
                                        </td>
                                        <td className="d-flex align-items-center justify-content-evenly">

                                            <button className="btn-sm text-white" style={{backgroundColor: "#0DCAF0"}}
                                                    onClick={()=>history.push(`/products-crud/${product.id}`)}
                                            >
                                                <VisibilityIcon/>
                                            </button>

                                            <button className="btn-sm btn-danger"
                                                    onClick={() => setDeleteProductFunction(product)}>
                                                <DeleteForeverIcon/>
                                            </button>
                                            <button className="btn-sm btn-warning mt-1"
                                                    onClick={() => setUpdateProductFunction(product)}>
                                                <ModeEditIcon/>
                                            </button>
                                        </td>
                                    </tr> )) :

                                    <tr>
                                        <td colSpan={5}>Mahsululot topilmadi !!!</td>
                                    </tr>

                        }
                        </tbody>

                    </table>
                </div>

                {
                    products.length !==0 ? <CustomPagination current_page={current_page} setCurrentPage={setCurrentPage}
                                                                       numberOfPages={numberOfPages}/> : ""
                }

            </div>

            <CreateProduct current_page={current_page} limit={limit} add_open_modal={add_open_modal} toggle={toggle}
                           closeAddModalFunction={closeAddModalFunction}/>

            <DeleteProduct limit={limit} setCurrentPage={setCurrentPage} />

            <UpdateProduct limit={limit} current_page={current_page}/>

        </Layout>
    );
};

export default ProductsCrud;