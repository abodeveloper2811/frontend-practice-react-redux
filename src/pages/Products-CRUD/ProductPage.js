import React, {useEffect} from 'react';
import Layout from "../../components/Layout/Layout";
import {useParams, useHistory} from 'react-router-dom';
import {useDispatch, useSelector} from "react-redux";
import {getProduct} from "../../redux/actions/productsActions";
import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import Skeleton, { SkeletonTheme } from 'react-loading-skeleton';
import CurrencyFormat from 'react-currency-format';

const ProductPage = () => {

    const dispatch = useDispatch();
    const history = useHistory();
    const {id} = useParams();
    const {product, loading} = useSelector(state => state.products);

    useEffect(()=>{
        dispatch(getProduct(id));
    },[id]);

    return (
        <Layout>

            <div className="Product-page">

                <div className="row">
                    <div className="col-md-12">
                        <div className="d-flex mb-4 align-items-center justify-content-between">
                            <h1 className="text-info">Mahsulot sahifasi</h1>
                            <button className="btn btn-info text-white" onClick={()=>history.goBack()}>Orqaga qaytish <ArrowBackIcon/></button>
                        </div>
                    </div>
                </div>


                {
                    !loading ?
                        <div className="row">
                            <div className="col-md-6">
                                <div className="card border-0">
                                    <div className="card-header border-4 border border-info p-0">
                                        <img src={product.image} className="img-fluid" alt=""/>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-6">
                                <div className="card border border-info border-2">
                                    <div className="card-body">
                                        <div style={{fontSize: "20px"}}> <b>Mahsulot nomi:</b> {product.name} </div>
                                        <div style={{fontSize: "20px"}}> <b>Mahsulot fotmati:</b> {product.format} </div>
                                        <div style={{fontSize: "20px"}}> <b className="me-1">Mahsulot narxi:</b>

                                            <CurrencyFormat
                                                thousandSpacing="3"
                                                displayType="text"
                                                value={product.price}
                                                thousandSeparator={' '}
                                                suffix={" so'm"}
                                            />

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> :

                        <SkeletonTheme baseColor="#0DCAF0" highlightColor="#ffffff">
                            <div className="row">
                                <div className="col-md-6">
                                    <div className="card p-0">
                                        <div className="card-header bg-info p-0">
                                            <Skeleton count={1} height={"400px"}/>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-md-6">
                                    <div className="card  border border-info border-2">
                                        <div className="card-body">
                                            <Skeleton count={3}/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </SkeletonTheme>
                }

            </div>

        </Layout>
    );
};

export default ProductPage;